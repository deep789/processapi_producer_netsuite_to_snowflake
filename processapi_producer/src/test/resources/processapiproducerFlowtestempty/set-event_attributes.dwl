{
  "headers": {
    "user-agent": "PostmanRuntime/7.28.0",
    "accept": "*/*",
    "postman-token": "8c562a99-04fb-4cc5-8199-f2119caa38cc",
    "host": "localhost:8081",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "0"
  },
  "clientCertificate": null,
  "method": "POST",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/producer",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/producer",
  "relativePath": "/producer",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {},
  "rawRequestUri": "/producer",
  "rawRequestPath": "/producer",
  "remoteAddress": "/127.0.0.1:56310",
  "requestPath": "/producer"
}
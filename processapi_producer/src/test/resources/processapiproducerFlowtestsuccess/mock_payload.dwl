[
  {
    "lastName": "Weiss",
    "isInactive": false,
    "lastModifiedDate": "2021-03-27T13:05:50",
    "externalId": "BN1",
    "entityId": "Brian Weiss",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "5",
      "name": "Brilliant Night"
    },
    "internalId": "2439",
    "firstName": "Brian",
    "dateCreated": "2021-03-17T15:50:35",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "email": "brianjweiss@yahoo.com",
    "supervisor": {
      "externalId": null,
      "type": null,
      "internalId": "914",
      "name": "Scott Beveridge"
    },
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  },
  {
    "lastName": "Mixer",
    "isInactive": false,
    "lastModifiedDate": "2021-03-27T13:05:46",
    "externalId": "BN3",
    "entityId": "Amy Mixer",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "5",
      "name": "Brilliant Night"
    },
    "internalId": "2449",
    "firstName": "Amy",
    "dateCreated": "2021-03-27T12:29:14",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "email": "AmyMixer@brilliantnight.com",
    "supervisor": {
      "externalId": null,
      "type": null,
      "internalId": "2439",
      "name": "Brian Weiss"
    },
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  },
  {
    "lastName": "Houzz",
    "isInactive": false,
    "lastModifiedDate": "2021-03-27T13:05:50",
    "externalId": "BN8",
    "entityId": "Bern Houzz",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "5",
      "name": "Brilliant Night"
    },
    "internalId": "2450",
    "firstName": "Bern",
    "dateCreated": "2021-03-27T12:29:15",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "email": "BernHouzz@brilliantnight.com",
    "supervisor": {
      "externalId": null,
      "type": null,
      "internalId": "2439",
      "name": "Brian Weiss"
    },
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  },
  {
    "lastName": "Burz",
    "isInactive": false,
    "lastModifiedDate": "2021-03-27T13:05:48",
    "externalId": "BN6",
    "entityId": "Ashley Burz",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "5",
      "name": "Brilliant Night"
    },
    "internalId": "2451",
    "firstName": "Ashley",
    "dateCreated": "2021-03-27T12:29:15",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "email": "AshleyBurz@brilliantnight.com",
    "supervisor": {
      "externalId": null,
      "type": null,
      "internalId": "2439",
      "name": "Brian Weiss"
    },
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  },
  {
    "lastName": "Fisher",
    "isInactive": false,
    "lastModifiedDate": "2021-03-27T13:05:49",
    "externalId": "BN7",
    "entityId": "Bass Fisher",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "5",
      "name": "Brilliant Night"
    },
    "internalId": "2452",
    "firstName": "Bass",
    "dateCreated": "2021-03-27T12:29:16",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "email": "BassFisher@brilliantnight.com",
    "supervisor": {
      "externalId": null,
      "type": null,
      "internalId": "2439",
      "name": "Brian Weiss"
    },
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  },
  {
    "lastName": "Ales",
    "isInactive": false,
    "lastModifiedDate": "2021-03-27T13:05:47",
    "externalId": "BN4",
    "entityId": "Angie Ales",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "5",
      "name": "Brilliant Night"
    },
    "internalId": "2453",
    "firstName": "Angie",
    "dateCreated": "2021-03-27T12:29:16",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "email": "AngieAles@brilliantnight.com",
    "supervisor": {
      "externalId": null,
      "type": null,
      "internalId": "2439",
      "name": "Brian Weiss"
    },
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  },
  {
    "lastName": "Mith",
    "isInactive": false,
    "lastModifiedDate": "2021-03-27T13:05:48",
    "externalId": "BN5",
    "entityId": "Ari Mith",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "5",
      "name": "Brilliant Night"
    },
    "internalId": "2454",
    "firstName": "Ari",
    "dateCreated": "2021-03-27T12:29:16",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "email": "AriMith@brilliantnight.com",
    "supervisor": {
      "externalId": null,
      "type": null,
      "internalId": "2439",
      "name": "Brian Weiss"
    },
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  },
  {
    "lastName": "Wise",
    "isInactive": false,
    "lastModifiedDate": "2021-03-27T13:09:08",
    "externalId": "BN2",
    "entityId": "Shan Wise",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "5",
      "name": "Brilliant Night"
    },
    "internalId": "2455",
    "firstName": "Shan",
    "dateCreated": "2021-03-27T12:29:16",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "email": "ShanWise@brilliantnight.com",
    "supervisor": {
      "externalId": null,
      "type": null,
      "internalId": "2439",
      "name": "Brian Weiss"
    },
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  },
  {
    "lastName": "M",
    "isInactive": false,
    "lastModifiedDate": "2021-05-24T00:12:20",
    "entityId": "mani M",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "1",
      "name": "Honeycomb Mfg."
    },
    "internalId": "2551",
    "firstName": "mani",
    "dateCreated": "2021-05-03T22:07:48",
    "officePhone": "234-5436",
    "mobilePhone": "(789) 012-3456",
    "phone": "(901) 234-5776",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "salutation": "mani",
    "fax": "123489076",
    "email": "manideep.meda@infometry.net",
    "supervisor": {
      "externalId": null,
      "type": null,
      "internalId": "24",
      "name": "Carmen Matthews"
    },
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  },
  {
    "lastName": "Padmakumar",
    "isInactive": false,
    "lastModifiedDate": "2021-05-17T23:20:11",
    "entityId": "Gokul Padmakumar",
    "isPrivate": false,
    "subsidiary": {
      "externalId": null,
      "type": null,
      "internalId": "4",
      "name": "test sub"
    },
    "internalId": "2552",
    "firstName": "Gokul",
    "dateCreated": "2021-05-03T23:18:29",
    "phone": "+91 9037010706",
    "globalSubscriptionStatus": "SOFT_OPT_IN",
    "salutation": "MR",
    "email": "gokul.padmakumar@infometry.net",
    "customFieldList": {
      "customField": [
        {
          "StringCustomFieldRef__custentity23": "ERROR: Field 'internalid' Not Found"
        }
      ]
    }
  }
]
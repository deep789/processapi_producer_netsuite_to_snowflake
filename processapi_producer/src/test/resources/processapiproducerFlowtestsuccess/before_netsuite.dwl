%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo(
{
	"keyName" : "lastModifiedDate" as String,
	"objectName" : "ContactSearchBasic" as String,
	searchValue : vars.lastExec,
	operator : "AFTER" as String,
})